import { Button } from "@/components/button";

export default function Home() {
  return (
    <>
      <img className="absolute w-full h-full object-cover object-center" src="/bg.gif" />
      <div className="absolute w-full h-full bg-neutral-950/80" />
      <main className="fixed w-full h-full flex flex-col justify-center items-center backdrop-blur">
        <img className="w-[24rem] h-[8rem] object-cover object-center" src="/98f849cb-38cf-4b23-ac96-e18c80474591.gif" />
        <Button />
      </main>
    </>
  );
}
