import { load } from "cheerio";

export async function GET() {
  const res = await fetch("https://eu.hitmotop.com/search?q=kai+angel");
  const body = await res.text();
  const $ = load(body);

  const tracks: string[] = [];

  $(".tracks__item").each((_, element) => {
    const trackImg = $(element)
      .find(".track__img")
      .css("background-image")
      ?.replace(/url\(['"]?(.*?)['"]?\)/, "$1");
    const downloadLink = $(element).find(".track__download-btn").attr("href");

    if (trackImg && downloadLink) tracks.push(downloadLink);
  });

  const randomLink = tracks[Math.floor(Math.random() * (tracks.length - 1))];
  const response = await fetch(randomLink);

  const stream = new ReadableStream({
    async start(controller) {
      const reader = (response.body as any).getReader();

      try {
        while (true) {
          const { done, value } = await reader.read();

          if (done) {
            break;
          }

          controller.enqueue(value);
        }
      } finally {
        reader.releaseLock();
        controller.close();
      }
    },
  });

  return new Response(stream, {
    headers: {
      "Content-Type": "audio/mpeg",
    },
  });
}

export const dynamic = "force-dynamic";
