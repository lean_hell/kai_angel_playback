"use client";

import { useEffect, useRef, useState } from "react";

export function Button() {
  const [loading, setLoading] = useState(false);
  const audioRef = useRef<HTMLAudioElement | null>(null);

  const playNewTrack = async () => {
    if (loading) return;
    setLoading(true);
    audioRef.current?.load();
  };

  useEffect(() => {
    if (audioRef.current) audioRef.current.onplay = () => setLoading(false);
  }, []);

  return (
    <>
      <button
        onClick={playNewTrack}
        className="w-[18rem] h-[2.5rem] flex justify-center items-center font-bold bg-neutral-100 text-neutral-950 rounded-xl hover:bg-neutral-400 duration-200"
      >
        {loading ? <div className="w-[20px] h-[20px] border-[2px] border-neutral-900 border-t-neutral-300 rounded-full animate-spin " /> : "Помазаться говном "}
      </button>
      <audio className="hidden" ref={audioRef} src="/api" autoPlay controls />
    </>
  );
}
